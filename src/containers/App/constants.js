export const SET_LOCAL = 'Language/SET_LOCAL';
export const SET_THEME = 'Language/SET_THEME';
export const SET_ADVICE = 'APP/SET_ADVICE';
export const GET_ADVICE = 'APP/GET_ADVICE';
