import { produce } from 'immer';

import { SET_LOCAL, SET_THEME, SET_ADVICE } from '@containers/App/constants';

export const initialState = {
  locale: 'id',
  theme: 'dark',
  advice: {},
};

export const storedKey = ['locale', 'theme', 'advice'];

const appReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_LOCAL:
        draft.locale = action.locale;
        break;
      case SET_THEME:
        draft.theme = action.theme;
        break;
      case SET_ADVICE:
        draft.advice = action.advice;
    }
  });

export default appReducer;
