import { takeLatest, call, put } from 'redux-saga/effects';
import { setAdvice } from '@containers/App/actions';
import { GET_ADVICE } from '@containers/App/constants';

import { getApiAdviceList } from '@domain/api';

export function* doGetAdviceList() {
  try {
    const adviceList = yield call(getApiAdviceList);
    if (adviceList) {
      yield put(setAdvice(adviceList.slip));
    }
  } catch (error) {
    console.error(error);
  }
}

export default function* appSaga() {
  yield takeLatest(GET_ADVICE, doGetAdviceList);
}
