import axios from 'axios';

const urls = {
  adviceList: 'https://api.adviceslip.com/advice',
};

export const callAPI = async (endpoint, method, headers = {}, params = {}, data = {}) => {
  const options = {
    url: endpoint,
    method,
    headers,
    data,
    params,
  };

  return axios(options).then((response) => {
    const responseAPI = response.data;
    return responseAPI;
  });
};

export const getApiAdviceList = () =>
  callAPI(urls.adviceList, 'get', {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  });
