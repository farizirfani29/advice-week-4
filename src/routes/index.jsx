import MainLayout from '@layouts/MainLayout';

// import Home from '@pages/Home';
import Advice from '@pages/advice';
import NotFound from '@pages/NotFound';

const routes = [
  {
    path: '/',
    name: 'Advice',
    component: Advice,
    // layout: MainLayout,
  },

  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;
